## Electronics component detection using DNNs

The detection algorithm is built around a Single Shot Detector (SSD) DNN. [The architecture](https://arxiv.org/abs/1512.02325) by Wei Liu et al. is reproduced in a Keras environment.

In the src directory you can find comprehensive instructions on how to configure the machine before beginning any inference job or training instance.

# Inference
To launch inference just run the launch_ssd.py from a bash terminal as follows:
```./launch_ssd.py```

A --debug flag can be passed to the script to enable real-time visualization of detections
```./launch_ssd.py --debug```

A --watchdog flag can also be enabled to stop the execution after 1000 frames (or any arbitrary value stored in the stopframe variable)
```./launch_ssd.py --watchdog```

# Training
To launch a new training of the model you can use the ssd_300_training.py file. A complete dataset for fine tuning purposes may be downloaded from [here](https://Mediate-Srl.quickconnect.to/d/s/466468803415387925/krCvJtCt16-TpSdMg1Chmf426Fg_f188-US.gAxQXhgY_).
