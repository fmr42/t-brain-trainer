# imagenet-download
Requires python 2.7

Python scripts to download imagenet images and pre-proccess them

example usage:

 ./imagenetDownloader.py  n02955247 ../dataset --humanreadable -F --images=1000 --minsize=7000 -j20
 
In order to download 1000 images from any category under any subtree (-F) of the capacitors (n02955247), running 20 threads in parallel, and only downloading images larger than 7kB.
