#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# In[0]:
import os
#import socket
import argparse
import pyrealsense2 as rs

import cv2
import datetime
import numpy as np
from matplotlib import cm

from keras import backend as K
from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from tensorflow.python.client import device_lib

def build_ssd(weights_path,img_height,img_width):
    from keras.optimizers import Adam
    
    nclasses=4
    classes = ['background',
               'cilindro', 'parallelepipedo', 'caramella', 'ceramici']
    
    # Build the Keras model
    K.clear_session() # Clear previous models from memory.
        
    model = ssd_300(image_size=(img_height, img_width, 3),
                    n_classes=nclasses, # 4 nominal
                    mode='inference', # nominal: 'inference' or 'inference_fast'
                    l2_regularization=0.0005,
                    scales=[0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05], 
                    aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5]],
                    two_boxes_for_ar1=True,
                    steps=[8, 16, 32, 64, 100, 300],
                    offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                    clip_boxes=False,
                    variances=[0.1, 0.1, 0.2, 0.2],
                    normalize_coords=True,
                    subtract_mean=[123, 117, 104],
                    swap_channels=[2, 1, 0],
                    confidence_thresh=0.5,
                    iou_threshold=0.45,
                    top_k=200,
                    nms_max_output_size=400)
    
    # Load the trained weights into the model.
    model.load_weights(weights_path, by_name=True)
    
    #Compile the model so that Keras won't complain the next time you load it.   
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    
    model.compile(optimizer=adam, loss=ssd_loss.compute_loss)
        
    print('Model built.')    
    return model, classes

# In[1]:
""" Set main parameters for the inference """
parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')
parser.add_argument('--watchdog', action='store_true')
args = parser.parse_args()

# Set flags to tune the behaviour of the inference
# args.debug -> if true enable the real time display of predictions and further cmd line diagnostics. Nominal: False
    
# if true set a watchdog and stop execution after stopframe frames
if args.watchdog:
    stopframe=1000
    
# Set main parameters of the model
img_height=300 # set the format of the input images of the model. Nominal: 300
img_width=300 # Nominal: 300

# Load the trained weights from
weights_path = '/home/tommaso/ssd_keras_data/checkpoints_tera_ann/ssd300_tera_epoch-42_loss-2.7557_val_loss-3.8633.h5'
# Build a model using some pretrained weights
model, classes = build_ssd(weights_path,img_height,img_width)

# see below: `y_pred` contains a fixed number of predictions per batch item, many of which are low-confidence predictions 
# or dummy entries. We therefore need to apply a confidence threshold to filter out the bad predictions. 
confidence_threshold=0.75 # Nominal: 0.75

# Set main parameters of the realsense pipeline
width=1920 # image width of the realsense camera. Nominal: 1920
height=1080 # image height of the realsense camera. Nominal: 1080
rsfps=30 # fps target for realsense camera. Nominal: 30

# Select the device that will run ANN computations. On a single GPU machine 0 runs on the local GPU-0 
# while 1 will use the system CPU
os.environ["CUDA_VISIBLE_DEVICES"]="0" # nominal: "0"

# Print wich device has been selected
print('Environment configured. Inference running on:')
print(device_lib.list_local_devices())

# In[2]:
# Create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
#client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
## Connect the client ((target, port))
#client.connect(('192.168.1.2', 1026))
#print('TCP socket initialized.')

# In[3]:
# Initialize realsense pipeline

# Configure color (and optionally the depth) streams
pipeline = rs.pipeline()
config = rs.config()

# config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30) # Nominal: 640x480 z16 30fps
config.enable_stream(rs.stream.color, width, height, rs.format.bgr8, rsfps) # Nominal: 1920x18080 bgr8 30fps
 
# Start camera streaming
pipeline.start(config)
print('Camera pipeline started.')

# In[4]: 
""" Main """
print('Launching inference...')
       
# Acquire images from a camera or from a video
start_time = datetime.datetime.now()
num_frames = 0

if args.debug:
    # Create an empty cv2 window that will display the predictions
    cv2.namedWindow('SSD viewer', cv2.WINDOW_NORMAL) 

while True:    
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()
    # depth_frame = frames.get_depth_frame()
    
    color_frame = frames.get_color_frame()
    if not color_frame:
        continue
    
    # Convert images to numpy arrays
    image_np = np.asanyarray(color_frame.get_data())
    # depth_image = np.asanyarray(depth_frame.get_data())
    
    if args.debug:
        image_np = cv2.flip(image_np, 1)
        
    try:
        image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    except:
        print("Error converting to RGB")

    # Resize the image to meet ANN input requirements
    resized_img=np.expand_dims(cv2.resize(image_np,(img_height, img_width)),0)
        
    # Make predictions with the model
    y_pred = model.predict(resized_img)    
    
    y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]
    
    if args.debug:
        np.set_printoptions(precision=2, suppress=True, linewidth=90)
        print("Predicted boxes:\n")
        print('   class   conf xmin   ymin   xmax   ymax')
        print(y_pred_thresh[0])
    
    if y_pred_thresh[0].shape[1] < 1:
        # If the model does not predict any high confidence predictions skip iteration
        pass

    else:   
        # Visualize the predictions on the original full-resolution image
        if args.debug:
            for box in y_pred_thresh[0]:
                xmin = (box[2] / img_width) * width
                ymin = (box[3] / img_height) * height
                xmax = (box[4] / img_width) * width
                ymax = (box[5] / img_height) * height
                color = cm.jet(int(box[0])) #colors[int(box[0])]
                label = '{}: {:.2f}'.format(classes[int(box[0])], box[1])
                cv2.rectangle(image_np, (int(xmin),int(ymin)), (int(xmax), int(ymax)), (color), thickness=3,lineType=8)
                cv2.putText(image_np, label, (int(xmin), int(ymin-10)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (color), 2)

#            # Send detected bboxes to the server using TCP
#            client.send(y_pred_thresh)
#            # Optionally check the server received data (buffer size: 4096)
#            response = client.recv(4096)

    # Calculate and display frames per second (FPS)
    num_frames += 1    
    if args.debug:
        elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
        fps='Frame number: '+str(num_frames)+' FPS: '+str(num_frames / elapsed_time-int(5))[0:5]
        cv2.putText(image_np, fps, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (77, 255, 9), 2)                
        cv2.imshow('SSD viewer', cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))
    else:
        if num_frames % 200 == 0:
            elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
            fps='Running... Frame number: '+str(num_frames)+' FPS: '+str(num_frames / elapsed_time-int(5))[0:5]
            print(fps)
        elif num_frames==1:
            print('Running... Waiting for data to print diagnostics.')

    # Flush cv2 window if q key is pressed
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
    
    if args.watchdog:
        # Stop execution after stopframe (1000) frames
        if num_frames==stopframe: 
            pipeline.stop() # stop realsense pipeline and make the device available
#            client.shutdown() # notify the server before closing the client socket
#            client.close() # close TCP socket

            # Confirm graceful exit
            print('Graceful stop.')
            break
            
# Confirm exit
print('Stopped.')
