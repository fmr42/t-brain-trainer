## Environment-machine setup
First disable secure boot in your motherboard settings. 
The system is meant to run on Ubuntu 16.04.5 LTS (xenial)
OS iso available at: http://releases.ubuntu.com/16.04/

# Install GPU drivers
Install the GPU drivers using the Ubuntu system settings gui or via command line. The target driver version is: nvidia graphics driver 396.X or above
CLI instructions:
```
sudo apt purge nvidia*
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
sudo apt install nvidia-driver-396 
```
A terminal GPU monitor ([nvidia-smi](https://developer.nvidia.com/nvidia-system-management-interface)) is available running:
```./gpu_monitor```

# Install CUDA and cuDNN
LibCuDnn is available to download from [here](https://Mediate-Srl.quickconnect.to/d/s/470092435861548885/p-JrOtV1kvqYs-zngIDy8QF-DHXO9205-WC-AoNwahgY_).

Download and install CUDA 9.0
```wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run```

add executable bit to CUDA run file
```chmod +x cuda_9.0.176_384.81_linux-run```

extract CUDA in the home folder
```./cuda_9.0.176_384.81_linux-run --extract=$HOME```

install the CUDA toolkit
```sudo ./cuda-linux.9.0.176-22781540.run```

Check if the installation was completed succesfully by running examples
```sudo ./cuda-samples.9.0.176-22781540-linux.run```

Configure the runtime library
```sudo bash -c "echo /usr/local/cuda/lib64/ > /etc/ld.so.conf.d/cuda.conf"```
```sudo ldconfig```

append to the system path nvcc
```sudo nano /etc/environment```

then add :/usr/local/cuda/bin at the end of the system path inside the quotes

Optional tests. After system reboot navigate to the cuda samples directory:
```cd /usr/local/cuda-9.0/samples```
```sudo make```

Compiling the samples can take significant time, some warnings may be raised but can be ignored if related to old instruction sets and so on. After that you can run tests:
```cd /usr/local/cuda/samples/bin/x86_64/linux/release```
```./deviceQuery```

At the very bottom something similar should appear:
```deviceQuery, CUDA Driver = CUDART, CUDA Driver Version = 9.0, CUDA Runtime Version = 9.0, NumDevs = 1```
```Result = PASS```

# Install Anaconda
Download and install Anaconda distribution from here https://www.anaconda.com/download/#linux
The original implementation used anaconda3 version 5.3.0, available to download from [here](https://Mediate-Srl.quickconnect.to/d/s/470092435861548885/p-JrOtV1kvqYs-zngIDy8QF-DHXO9205-WC-AoNwahgY_).
```chmod +x ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh```
```sudo ./Anaconda3-5.3.0-Linux-x86_64.sh```

Create a dedicated virtual environment loading the configuration from file
```conda env create -f ~/tera_mediate_ssd/src/kerastf.yml```

Run tests to verify that the environment configuration was correctly executed:
```
cd ~/tera_mediate_ssd/src
conda activate kerastf
./env_test.py
```

If the test is executed correctly you should see this message: Environment test: PASSED.

If launching inference, right after a setup from scratch, raises some opencv3 errors:
```
conda activate kerastf
conda install -c menpo opencv3
```
which contains precompiled opencv3 libs.
