## D435 Camera presets

Given a certain task, optimization of camera acquisition and post-processing settings may vary. [Here](https://github.com/IntelRealSense/librealsense/wiki/D400-Series-Visual-Presets#short-range-preset) a comprehensive list of available tested presets. 

# Ad hoc optimization
Further information on how to modify and optimize those parameters can be found [here](https://realsense.intel.com/intel-realsense-downloads/#whitepaper).


