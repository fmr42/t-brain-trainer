# Install Intel realsense lib
Navigate to the home directory. Get the main repo from github:
```git clone https://github.com/IntelRealSense/librealsense.git```

Update Ubuntu distro and kernels:
```sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade```

UNPLUG ANY CONNECTED REALSENSE CAMERA

Navigate to the main realsense directory
```cd /realsense```

Install the core packages required to build librealsense binaries and the affected kernel modules:
```sudo apt-get install git libssl-dev libusb-1.0-0-dev pkg-config libgtk-3-dev```

Install some distribution-specific packages: 
Ubuntu 16:
```sudo apt-get install libglfw3-dev```

Ubuntu 18:
```sudo apt-get install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev```

Install Intel Realsense permission scripts located in librealsense source directory:
```sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/```
```sudo udevadm control -R && udevadm trigger```

Build and apply patched kernel modules for:
* Ubuntu 14/16/18 with LTS kernel script will download, patch and build realsense-affected kernel modules (drivers).
Then it will attempt to insert the patched module instead of the active one. If failed the original uvc modules will be restored.

```./scripts/patch-realsense-ubuntu-lts.sh```

Navigate to librealsense root directory and run 
```mkdir build && cd build```

If needed install cmake
```sudo apt install cmake``` 

Build with optimizations.
```cmake ../ -DCMAKE_BUILD_TYPE=Release``` 

Recompile and install librealsense binaries:
```sudo make uninstall && make clean && make && sudo make install```

Tip: Use make -jX for parallel compilation, where X stands for the number of CPU cores available:
```sudo make uninstall && make clean && make **-j8** && sudo make install```

Note: Linux build configuration is presently configured to use the V4L2 backend by default.
Note: If you encounter the following error during compilation gcc: internal compiler error it might indicate that you do not have enough memory or swap space on your machine. Try closing memory consuming applications, and if you are running inside a VM increase available RAM to at least 2 GB.

# Test Installation
To test the installation, connect a realsense camera then open a terminal and run: 
```realsense-viewer``` 

You should be able to see RGB and depth streams from the camera.

If an import error is raised during the setup import of pyrealsense2 module for inference, try these steps:
```conda activate kerastf```
```pip install pyrealsense2```



